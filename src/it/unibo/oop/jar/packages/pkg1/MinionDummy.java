/**
 * 
 */
package it.unibo.oop.jar.packages.pkg1;

/**
 * Dummy class, just to try by-hand compilation.
 * 
 */
public class MinionDummy extends DummyClass {

    @Override
    public String toString() {
        return "MinionDummy";
    }

}
